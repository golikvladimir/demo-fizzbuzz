public class FizzBuzz {
    public static void main(String[] args) {
        int[] numbers = new int[100];

        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = (int) (Math.random() * 100);

            System.out.printf("%s\t%s%s\n",
                    numbers[i],
                    numbers[i] % 3 == 0 && numbers[i] != 0 ? "fizz" : "",
                    numbers[i] % 7 == 0 && numbers[i] != 0 ? "buzz" : "");
        }
    }
}
